DELIMITER $$

CREATE TRIGGER t_log_m_server_update
	BEFORE UPDATE
	ON m_server
FOR EACH ROW BEGIN
	DECLARE data VARCHAR(200);
    SET data = CONCAT(
		REPLACE(NEW.id, '|', '.'),
		'|',
		REPLACE(NEW.name, '|', '.'),
		'|',
		REPLACE(NEW.ip, '|', '.'),
		'|',
		REPLACE(NEW.port, '|', '.'),
		'|',
		REPLACE(NEW.type, '|', '.'),
		'|',
		REPLACE(NEW.description, '|', '.')
	);
	INSERT INTO m_server_log (ip, type, time, data)
	VALUES (NEW.ip, 'update', CURRENT_TIMESTAMP(), data);
END$$

DELIMITER ;

DELIMITER $$

CREATE TRIGGER t_log_m_server_insert
	BEFORE INSERT
	ON m_server
FOR EACH ROW BEGIN
	DECLARE data VARCHAR(200);
    SET data = CONCAT(
		REPLACE(NEW.id, '|', '.'),
		'|',
		REPLACE(NEW.name, '|', '.'),
		'|',
		REPLACE(NEW.ip, '|', '.'),
		'|',
		REPLACE(NEW.port, '|', '.'),
		'|',
		REPLACE(NEW.type, '|', '.'),
		'|',
		REPLACE(NEW.description, '|', '.')
	);
	INSERT INTO m_server_log (ip, type, time, data)
	VALUES (NEW.ip, 'insert', CURRENT_TIMESTAMP(), data);
END$$

DELIMITER ;

DELIMITER $$

CREATE TRIGGER t_log_m_server_delete
	BEFORE DELETE
	ON m_server
FOR EACH ROW BEGIN
	DECLARE data VARCHAR(200);
    SET data = CONCAT(
		REPLACE(OLD.id, '|', '.'),
		'|',
		REPLACE(OLD.name, '|', '.'),
		'|',
		REPLACE(OLD.ip, '|', '.'),
		'|',
		REPLACE(OLD.port, '|', '.'),
		'|',
		REPLACE(OLD.type, '|', '.'),
		'|',
		REPLACE(OLD.description, '|', '.')
	);
	INSERT INTO m_server_log (ip, type, time, data)
	VALUES (OLD.ip, 'delete', CURRENT_TIMESTAMP(), data);
END$$

DELIMITER ;

DELIMITER $$

CREATE TRIGGER t_d_url_insert
	AFTER INSERT
	ON d_url
FOR EACH ROW BEGIN
	INSERT INTO d_url_extend (id, last, touch)
	VALUES (NEW.id, CURRENT_TIMESTAMP(), 1);
END$$

DELIMITER ;

DELIMITER $$

CREATE TRIGGER t_d_url_extend_update
	AFTER UPDATE
	ON d_url_extend
FOR EACH ROW BEGIN
	DECLARE today DATETIME;
	DECLARE rowCount INTEGER;
	DECLARE currentCount INTEGER;
	SET rowCount = CURRENT_DATE();
	
	SELECT count(*) INTO rowCount
	FROM d_url_analyst WHERE `id` = NEW.id AND `time` = @today;
	
	IF @rowCount = 0 THEN
		-- insert new
		INSERT INTO d_url_analyst (id, time, count)
		VALUES (NEW.id, @today, 0);
	END IF;
	-- increment count
	SELECT `count` INTO currentCount
	FROM d_url_analyst WHERE `id` = NEW.id AND `time` = @today;
	
	INSERT INTO d_url_extend (id, last, touch)
	VALUES (NEW.id, @today, @currentCount + 1);
END$$

DELIMITER ;



















