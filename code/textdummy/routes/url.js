var express = require('express');
var router = express.Router();

import urlController from '../libs/sequelize/controller/url.controller'

// Get all url data
router.get('/', function (req, res, next) {
    urlController.findAll(req, res)
});

// Get url data by id
router.get('/:id', function (req, res, next) {
    urlController.find(req, res)
});

// Post url create
router.post('/', function (req, res, next) {
    urlController.create(req, res)
});

// Post url update
router.post('/:id', function (req, res, next) {
    urlController.update(req, res)
});

module.exports = router;
