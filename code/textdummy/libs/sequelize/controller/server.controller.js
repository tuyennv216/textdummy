import Op from 'sequelize'
import { models } from '../db'

import { mergeAttribute, mergeAttributeFn } from '../utils/help'

const create = (req, res) => {
    const attrs = ["created", "option", "data"]
    const server = {}
    attrs.forEach(item => mergeAttribute(server, item, req.body[item]))
 
    models.server.create(server).then(data => {
        res.send(data)
    })
}

const find = (req, res) => {
    const id = req.body.id

    models.server.findByPk(id).then(data => {
        res.send(data)
    })
}

const findAll = (req, res) => {
    const attrs = ["created", "option", "data"]
    const whereObject = {}
    attrs.forEach(item => mergeAttributeFn(whereObject, item, req.body[item], value => ({ [Op.like]: `%${value + ''}%` })))

    models.server.findAll({ where: whereObject }).then(data => {
        res.send(data)
    })
}

const update = (req, res) => {
    const attrs = ["id", "created", "option", "data"]
    const server = {}
    attrs.forEach(item => mergeAttribute(server, item, req.body[item]))

    models.server.update(server, {
        where: { id: server.id }
    }).then (effected => {
        res.send(effected)
    })
}

const remove = (req, res) => {
    const id = req.body.id
    models.server.destroy({
        where: { id: id }
    }).then(effected => {
        res.send(effected)
    })
}

export default {
    create,
    find,
    findAll,
    update,
    remove
}
