import { Op } from 'sequelize'
import db from '../db'

import { mergeAttribute, mergeAttributeFn } from '../utils/help'

const create = (req, res) => {
    const attrs = ["created", "option", "data"]
    const d_url = {}
    attrs.forEach(item => mergeAttribute(d_url, item, req.body[item]))

    db.models.d_url.create(d_url).then(data => {
        res.send(data)
    })
}

const find = (req, res) => {
    const id = req.body.id

    db.models.d_url.findByPk(id).then(data => {
        res.send(data)
    })
}

const findAll = (req, res) => {
    const attrs = ["created", "option", "data"]
    const whereObject = {}
    attrs.forEach(item => mergeAttributeFn(whereObject, item, req.query[item], value => ({ [Op.like]: `%${value + ''}%` })))

    db.models.d_url.findAll({ where: whereObject }).then(data => {
        res.send(data)
    })
}

const update = (req, res) => {
    const attrs = ["id", "created", "option", "data"]
    const d_url = {}
    attrs.forEach(item => mergeAttribute(d_url, item, req.body[item]))

    db.models.d_url.update(d_url, {
        where: { id: d_url.id }
    }).then(effected => {
        res.send(effected)
    })
}

const remove = (req, res) => {
    const id = req.body.id
    db.models.d_url.destroy({
        where: { id: id }
    }).then(effected => {
        res.send(effected)
    })
}

export default {
    create,
    find,
    findAll,
    update,
    remove
}
