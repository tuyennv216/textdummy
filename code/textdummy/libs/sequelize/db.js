import Promise, { reject } from 'bluebird'
import Sequelize from 'sequelize'
import config from './db.config'
import m_server from './models/m_server.model'
import m_server_log from './models/m_server_log.model'
import d_url from './models/d_url.model'
import d_url_extend from './models/d_url_extend.model'
import d_url_analyst from './models/d_url_analyst.model'
import { trigger, data } from './utils/initial/init_mysql'

const db = new Sequelize(config.db, config.user, config.password, {
    host: config.host,
    port: config.port,
    dialect: config.dialect,
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
    define: {
        freezeTableName: true,
        timestamps: false,
    },
})

m_server(db)
m_server_log(db)
d_url(db)
d_url_extend(db)
d_url_analyst(db)

// connect to db
export const connect = async () => {
    return new Promise((resolve, reject) => {
        try {
            db.authenticate()
                .then(function () {
                    console.log("DB connected successful!")
                })
                .catch(function (err) {
                    console.log("DB connected fail!")
                })

            db.sync({ force: false })
            resolve()
        } catch (e) {
            reject()
        }
    })
}

export const reset = async () => {
    try {
        db.authenticate()
            .then(async () => {
                console.log("DB connected successful!")
                // drop, create and insert new data
                await db.sync({ force: true })
                await trigger()
                await data()
                console.log("DB initial successful!")
                Promise.resolve()
            })
    } catch (e) {
        console.log("DB connected fail!")
        Promise.reject()
    }
}

export default db
