const { DataTypes } = require('sequelize')

module.exports = (sequelize) => {
    sequelize.define('m_server_log', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER
        },
        ip: {
            allowNull: false,
            type: DataTypes.STRING
        },
        type: {
            allowNull: false,
            type: DataTypes.STRING
        },
        time: {
            allowNull: false,
            type: DataTypes.DATE
        },
        data: {
            allowNull: false,
            type: DataTypes.STRING
        },
    }, {
        tableName: 'm_server_log',
    })
}
