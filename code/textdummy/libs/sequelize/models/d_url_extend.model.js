
const { DataTypes } = require('sequelize')

module.exports = (sequelize) => {
    sequelize.define('d_url_extend', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER,
            validate: {
                notEmpty: true
            }
        },
        last: {
            allowNull: false,
            type: DataTypes.DATE,
            validate: {
                isDate: true
            }
        },
        touch: {
            allowNull: false,
            type: DataTypes.INTEGER,
            defaultValue: 0,
            validate: {
                min: 0
            }
        },
    }, {
        tableName: 'd_url_extend',
    })
}
