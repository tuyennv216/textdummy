const { DataTypes } = require('sequelize')

module.exports = (sequelize) => {
    sequelize.define('m_server', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER,
            validate: {
                isInt: true,
                notNull: true,
            }
        },
        name: {
            allowNull: false,
            type: DataTypes.STRING,
            unique: true,
            validate: {
                notEmpty: true
            }
        },
        ip: {
            allowNull: false,
            type: DataTypes.STRING,
            validate: {
                isIP: true,
                notEmpty: true
            }
        },
        port: {
            allowNull: false,
            type: DataTypes.INTEGER,
            validate: {
                isInt: true
            }
        },
        type: {
            allowNull: false,
            type: DataTypes.STRING,
            unique: true,
            validate: {
                notEmpty: true
            }
        },
        description: {
            allowNull: false,
            type: DataTypes.STRING,
            validate: {
                notEmpty: true
            }
        },
    }, {
        tableName: 'm_server',
    })
}
