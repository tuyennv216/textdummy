const { DataTypes } = require('sequelize')

module.exports = (sequelize) => {
    sequelize.define('d_url_analyst', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER,
            validate: {
                notEmpty: true
            }
        },
        time: {
            allowNull: false,
            primaryKey: true,
            type: DataTypes.DATE,
            validate: {
                isDate: true
            }
        },
        count: {
            allowNull: false,
            type: DataTypes.INTEGER,
            defaultValue: 0,
            validate: {
                min: 0
            }
        },
    }, {
        tableName: 'd_url_analyst',
    })
}
