const { DataTypes } = require('sequelize')

module.exports = (sequelize) => {
    sequelize.define('d_url', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER,
            validate: {
                notEmpty: true
            }
        },
        created: {
            allowNull: false,
            type: DataTypes.DATE,
            validate: {
                isDate: true
            }
        },
        option: {
            allowNull: false,
            type: DataTypes.STRING,
            validate: {
                notEmpty: true
            }
        },
        data: {
            allowNull: false,
            type: DataTypes.STRING,
            validate: {
                notEmpty: true
            }
        },
    }, {
        tableName: 'd_url',
    })
}
