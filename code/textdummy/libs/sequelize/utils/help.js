exports.mergeAttribute = (target, name, value) => {
    if (value !== undefined) {
        target[name] = value
    }
}

exports.mergeAttributeFn = (target, name, value, fn) => {
    if (value !== undefined) {
        target[name] = fn(value)
    }
}
