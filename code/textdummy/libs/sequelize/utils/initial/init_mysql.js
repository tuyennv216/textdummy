import db from '../../db'

exports.trigger = async () => {
    await db.query(`
        CREATE TRIGGER t_log_m_server_update
            AFTER UPDATE
            ON m_server
        FOR EACH ROW BEGIN
            DECLARE data VARCHAR(200);
            SET data = CONCAT(
                REPLACE(NEW.id, '|', '.'),
                '|',
                REPLACE(NEW.name, '|', '.'),
                '|',
                REPLACE(NEW.ip, '|', '.'),
                '|',
                REPLACE(NEW.port, '|', '.'),
                '|',
                REPLACE(NEW.type, '|', '.'),
                '|',
                REPLACE(NEW.description, '|', '.')
            );
            INSERT INTO m_server_log (ip, type, time, data)
            VALUES (NEW.ip, 'update', CURRENT_TIMESTAMP(), data);
        END;
        `)
    await db.query(`
        CREATE TRIGGER t_log_m_server_insert
            AFTER INSERT
            ON m_server
        FOR EACH ROW BEGIN
            DECLARE data VARCHAR(200);
            SET data = CONCAT(
                REPLACE(NEW.id, '|', '.'),
                '|',
                REPLACE(NEW.name, '|', '.'),
                '|',
                REPLACE(NEW.ip, '|', '.'),
                '|',
                REPLACE(NEW.port, '|', '.'),
                '|',
                REPLACE(NEW.type, '|', '.'),
                '|',
                REPLACE(NEW.description, '|', '.')
            );
            INSERT INTO m_server_log (ip, type, time, data)
            VALUES (NEW.ip, 'insert', CURRENT_TIMESTAMP(), data);
        END;
        `)
    await db.query(`
        CREATE TRIGGER t_log_m_server_delete
            AFTER DELETE
            ON m_server
        FOR EACH ROW BEGIN
            DECLARE data VARCHAR(200);
            SET data = CONCAT(
                REPLACE(OLD.id, '|', '.'),
                '|',
                REPLACE(OLD.name, '|', '.'),
                '|',
                REPLACE(OLD.ip, '|', '.'),
                '|',
                REPLACE(OLD.port, '|', '.'),
                '|',
                REPLACE(OLD.type, '|', '.'),
                '|',
                REPLACE(OLD.description, '|', '.')
            );
            INSERT INTO m_server_log (ip, type, time, data)
            VALUES (OLD.ip, 'delete', CURRENT_TIMESTAMP(), data);
        END;
        `)
    await db.query(`
        CREATE TRIGGER t_d_url_insert
            AFTER INSERT
            ON d_url
        FOR EACH ROW BEGIN
            INSERT INTO d_url_extend (id, last, touch)
            VALUES (NEW.id, CURRENT_TIMESTAMP(), 1);
        END;
        `)
    await db.query(`
        CREATE TRIGGER t_d_url_extend_update
            AFTER UPDATE
            ON d_url_extend
        FOR EACH ROW BEGIN
            DECLARE today DATETIME;
            DECLARE rowCount INTEGER;
            DECLARE currentCount INTEGER;
            SET rowCount = CURRENT_DATE();
            
            SELECT count(*) INTO rowCount
            FROM d_url_analyst WHERE \`id\` = NEW.id AND \`time\` = @today;
            
            IF @rowCount = 0 THEN
                -- insert new
                INSERT INTO d_url_analyst (id, time, count)
                VALUES (NEW.id, @today, 0);
            END IF;
            -- increment count
            SELECT \`count\` INTO currentCount
            FROM d_url_analyst WHERE \`id\` = NEW.id AND \`time\` = @today;
            
            INSERT INTO d_url_extend (id, last, touch)
            VALUES (NEW.id, @today, @currentCount + 1);
        END;
        `)
    Promise.resolve()
}

exports.data = async () => {
    await db.query(`INSERT INTO m_server (\`name\`, \`ip\`, \`port\`, \`type\`, \`description\`) VALUES ('url', 'localhost', 4000, 'url', 'get url');`)
    await db.query(`INSERT INTO m_server (\`name\`, \`ip\`, \`port\`, \`type\`, \`description\`) VALUES ('text', 'localhost', 4001, 'txt', 'get text');`)
    await db.query(`INSERT INTO d_url (\`created\`, \`option\`, \`data\`) VALUES (CURRENT_TIMESTAMP(), 'len=100', 'abcdef123');`)
    await db.query(`INSERT INTO d_url (\`created\`, \`option\`, \`data\`) VALUES (CURRENT_TIMESTAMP(), 'len=50', 'hijklmno456');`)
    await db.query(`INSERT INTO d_url (\`created\`, \`option\`, \`data\`) VALUES (CURRENT_TIMESTAMP(), 'len=20', 'qrstuvwxyz789');`)
    Promise.resolve()
}
